function setTooltipTitle(trigger, title, action = 'show') {
  trigger
    .attr('title', title)
    .tooltip('fixTitle')
    .tooltip(action);
}

function showCopiedTooltip(e) {
  var trigger = $(e.trigger);
  var originalTitle = trigger.attr('data-original-title');

  setTooltipTitle(trigger, 'Copied');

  setTimeout(function () {
    setTooltipTitle(trigger, originalTitle, 'hide');
  }, 2000);
}

function initCopyButton(selector) {
  var clipboard = new Clipboard(selector);

  clipboard.on('success', showCopiedTooltip);

  return clipboard;
}

window.initCopyButton = initCopyButton;
